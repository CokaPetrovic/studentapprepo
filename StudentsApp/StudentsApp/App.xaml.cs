﻿using Prism.Events;
using StudentsApp.DataService;
using StudentsApp.ViewModel;
using System.Windows;

namespace StudentsApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //var bootstrapper = new Bootstrapper();
            //var container = bootstrapper.Bootstrap();


            //idemo u NuGet packagei uzmemo AutoFac. Idemo u startup folder i 
            //kreiramo klasu bootstrapper, public class
            //ovde se kreira autofac containter koji je zaduzen za kreiranje instanci
            //metod bootstrap---tu kreiramo containerBuilder - I want to use FriendDataService whenever IFriendDataService is required
            //za mainwindow i mainmodel as self
            IEventAggregator ag = new EventAggregator();
            var mainWindow = new MainWindow(new MainViewModel(ag,new NavigationViewModel(ag, new StudentDataService())));
            mainWindow.Show(); 
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            //ovo smo sami uradili da ne bude has stopped working...
            MessageBox.Show("Unexpected error occured. Please inform admin. "
                + System.Environment.NewLine + e.Exception.Message, "Unexpected error");
            e.Handled = true;
        }
    }
}

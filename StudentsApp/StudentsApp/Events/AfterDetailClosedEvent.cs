﻿using Prism.Events;

namespace StudentsApp.Events
{
    public class AfterDetailClosedEvent :PubSubEvent<AfterDetailClosedEventArgs>
    {
    }
}
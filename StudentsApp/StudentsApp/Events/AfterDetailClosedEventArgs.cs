﻿using Prism.Events;

namespace StudentsApp.Events
{
    public class AfterDetailClosedEventArgs
    {
        public int Id { get; set; }
        public string ViewModelName { get; set; }
    }
}
﻿using Prism.Events;

namespace StudentsApp.Event
{
    public class AfterDetailSavedEvent : PubSubEvent<AfterDetailSavedEventArgs>
    {

    }

    public class AfterDetailSavedEventArgs
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}

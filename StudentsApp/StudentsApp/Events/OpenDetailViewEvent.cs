﻿using Prism.Events;

namespace StudentsApp.Events
{
    public class OpenDetailViewEvent : PubSubEvent<OpenDetailViewEventArgs>
    {

    }

    public class OpenDetailViewEventArgs
    {
        public int Id { get; set; }
    }
}

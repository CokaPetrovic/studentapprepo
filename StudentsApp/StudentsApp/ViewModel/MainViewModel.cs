﻿using Prism.Commands;
using Prism.Events;
using StudentsApp.DataService;
using StudentsApp.Events;
using StudentsApp.ViewModel;
using StudentsApp.Wrapper;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace StudentsApp
{
    public class MainViewModel : ViewModelBase
    {

        private static int nextNewItemId = 0;
        private Prism.Events.IEventAggregator _eventAggregator;

        public StudentWrapper Student { get; set; }

        private StudentDataService _service;
        public INavigationViewModel NavigationViewModel { get; }
        public ICommand AddNewStudentCommand { get; }
        public ICommand OpenSingleDetailViewCommand { get; set; }
        public ICommand CreateNewDetailCommand { get; set; }
        public ObservableCollection<IStudentDetailViewModel> DetailViewModels { get; set; }

        public MainViewModel(IEventAggregator eventAggregator, INavigationViewModel navigationViewModel)
        { 
            DetailViewModels = new ObservableCollection<IStudentDetailViewModel>();
            AddNewStudentCommand = new DelegateCommand<Type>(OnCreateNewDetailExecute);
            OpenSingleDetailViewCommand = new DelegateCommand<Type>(OnOpenSingleDetailViewExecute);
            NavigationViewModel = navigationViewModel;
            _eventAggregator = eventAggregator;
            _eventAggregator.GetEvent<OpenDetailViewEvent>().Subscribe(OnOpenDetailViewAsync);
            _service = new StudentDataService();
            _eventAggregator.GetEvent<AfterDetailClosedEvent>().Subscribe(AfterDetailClosed);
            CreateNewDetailCommand = new DelegateCommand<Type>(OnCreateNewDetailExecute);

        }

        private void AfterDetailClosed(AfterDetailClosedEventArgs obj)
        {
            RemoveDetailViewModel(obj.Id, obj.ViewModelName);
        }

        private void RemoveDetailViewModel(int id, string viewModelName)
        {
            var detailViewModel = DetailViewModels
                 .SingleOrDefault(vm => vm.Id == id && vm.GetType().Name.Equals(viewModelName));
            if (detailViewModel != null)
            {
                DetailViewModels.Remove(detailViewModel);
            }
        }

        private async void OnOpenDetailViewAsync(OpenDetailViewEventArgs obj)
        {
            
            var detailViewModel = DetailViewModels
            .SingleOrDefault(vm => vm.Id== obj.Id);

            if (detailViewModel == null)
            {
                //ne postoji u obseravble collection i ne otvara se tab
                detailViewModel = new StudentDetailViewModel(_eventAggregator, _service);
                try
                {
                    await detailViewModel.LoadAsync(obj.Id);
                }
                catch
                {
                    MessageBox.Show("Data was deleted by another user");
                    await NavigationViewModel.LoadAsync();
                    return;
                }
                SelectedDetailViewModel = detailViewModel;
                DetailViewModels.Add(detailViewModel);
            }
            
            //SelectedDetailViewModel = detailViewModel;
            Student = (SelectedDetailViewModel as StudentDetailViewModel).Student;
        }

        private void OnOpenSingleDetailViewExecute(Type obj)
        {
            OnOpenDetailViewAsync(new OpenDetailViewEventArgs { Id = -1});
        }

        private void OnCreateNewDetailExecute(Type obj)
        {
            nextNewItemId--;
            OnOpenDetailViewAsync(new OpenDetailViewEventArgs { Id = nextNewItemId});
        }

        private IStudentDetailViewModel _selectedDetailViewModel;
        public IStudentDetailViewModel SelectedDetailViewModel
        {
            get
            { return _selectedDetailViewModel; }
            set//jer ocemo kroz tab control da menjamo
            {
                _selectedDetailViewModel = value;
                OnPropertyChanged();
            }
        }

        public async Task LoadAsync()//from dataService
        {
            await NavigationViewModel.LoadAsync();
        }
    }
}

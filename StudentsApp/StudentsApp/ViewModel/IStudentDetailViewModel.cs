﻿using System.Threading.Tasks;

namespace StudentsApp.ViewModel
{
    public interface IStudentDetailViewModel
    {
        int Id { get; set; }
        Task LoadAsync(int id);
    }
}
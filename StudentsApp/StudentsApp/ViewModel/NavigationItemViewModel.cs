﻿using Prism.Commands;
using Prism.Events;
using StudentsApp.Events;
using System.Windows.Input;

namespace StudentsApp.ViewModel
{
    public class NavigationItemViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public ICommand OpenDetailViewCommand { get; }
        private IEventAggregator _eventAggregator;

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public NavigationItemViewModel(IEventAggregator eventAggregator, int id, string name)
        {
            Id = id;
            Name = name;
            _eventAggregator = eventAggregator;
            OpenDetailViewCommand = new DelegateCommand(OnOpenDetailViewExecute);

        }

        private void OnOpenDetailViewExecute()
        {
            //todo: proeri da li je u opendetailviewEventArgs id isti kao this id, ako jeste ne otvaraj 
            //reseno u MainViewModel-u!!!

            _eventAggregator.GetEvent<OpenDetailViewEvent>().
             Publish(new OpenDetailViewEventArgs
             {
                 Id = this.Id
             }); ;
        
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsApp.ViewModel
{
    public interface INavigationViewModel
    {
        Task LoadAsync();
    }
}

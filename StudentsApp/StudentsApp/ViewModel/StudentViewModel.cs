﻿using StudentsApp.DataService;
using StudentsApp.Event;
using StudentsApp.Events;
using StudentsApp.Model;
using StudentsApp.Wrapper;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Prism.Events;
using Prism.Commands;


namespace StudentsApp.ViewModel
{
    public class StudentDetailViewModel : ViewModelBase, IStudentDetailViewModel
    {
        private StudentWrapper _student;
        public StudentWrapper Student
        {
            get { return _student; }
            set
            {
                _student = value;
                OnPropertyChanged();
            }
        }

        public bool HasChanges
        {
            get { return _hasChanges; }
            set
            {//only for real changes
                if (_hasChanges != value)
                {
                    _hasChanges = value;
                    OnPropertyChanged();
                    ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();
                }
            }
        }

        public ICommand CloseDetailViewCommand { get; set; }


        public ICommand SaveCommand { get; }
        public ICommand DeleteCommand { get; }
        public int Id { get; set; }

        public StudentDataService _service;

        private IEventAggregator _eventAggregator;
        private bool _hasChanges;

        public StudentDetailViewModel(IEventAggregator eventAggregator, StudentDataService service)
        {
            _service = service;
            _eventAggregator = eventAggregator;

            CloseDetailViewCommand = new DelegateCommand(OnCloseExecute);
            SaveCommand = new DelegateCommand(OnSaveCommandExecuteAsync, OnSaveCommandCanExecute);
            DeleteCommand = new DelegateCommand(OnDeleteExecute);



        }

        private async void OnSaveCommandExecuteAsync()
        {
            await _service.CreateOrUpdate(Student.Model);
            //Id = Student.Id;
            RaiseDetailSavedEvent(Student.Model.student_id);
            OnCloseExecute();
        }

        private async void OnDeleteExecute()
        {

            //dodaj pitanje messagebox
            await _service.Remove(Student.Model.student_id);
            RaiseDetailDeletedEvent(Student.Id);
            //publishujemo navigationViewModelu da treba da skine friend-a sa liste


        }

        private void RaiseDetailDeletedEvent(int id)
        {
            //publishuj event da navigationViewModel skloni iz Students studenta i ucitaj ponovo ali to sve tamo kad se catchuje event
            //sad samo proba
            _eventAggregator.GetEvent<AfterDetailDeletedEvent>().Publish(new AfterDetailDeletedEventArgs { Id = id });
            OnCloseExecute();
        }

        private bool OnSaveCommandCanExecute()
        {
            //todo validation
            return true;
        }

        protected virtual void RaiseDetailSavedEvent(int modelId)
        {
            _eventAggregator.GetEvent<AfterDetailSavedEvent>().Publish(
                new AfterDetailSavedEventArgs
                {
                    Id = modelId,
                    Name = Student.Name

                });
        }


        private void OnCloseExecute()
        {

            if (HasChanges)
            {
                var result = MessageBox.Show("You have made changes. Close this item?", "Question", MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
            }

            //close instance of detailviewModel tj moramo da publishujemo event afterclosed, a to pravimo kao novi event
            _eventAggregator.GetEvent<AfterDetailClosedEvent>()
                .Publish(new AfterDetailClosedEventArgs
                {
                    Id = this.Id,
                    ViewModelName = this.GetType().Name

                });
        }
        public async Task LoadAsync(int id)
        {

            var student = id > 0 ? await _service.GetByIdAsync(id)
                : await CreateNewStudent();
            //AKO NE POSTOJI MI KREIRAMO NOVOG, A NAPRAVILI SMO DA ID BUDE NULLABLE!!
            Id = id;
            InitializeStudent(student);


        }

        private void InitializeStudent(Student student)
        {
            Student = new StudentWrapper(student);
            Student.PropertyChanged += (s, e) =>
            {
                if (!HasChanges)
                {   //da ne bismo zvali svaki put nego samo kad nije true
                        // HasChanges = _service.HasChanges();
                }
                if (e.PropertyName == nameof(Student.HasErrors))
                {
                    ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();
                }

            };
            ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();

            //trick -> ako je id == 0 znaci da smo kreirali novog
            if (Student.Id == 0)
            {
                //setovanje propertija bilo kog ce triggerovati   validaciju tj SetValue iz ModelWrappera, zato radimo ovo ovde
                Student.Name = ""; //ovo ce trigovati validaciju
            }
        }


        private async Task<Student> CreateNewStudent()
        {
            var student = new Student();
            await _service.CreateOrUpdate(student);
            return student;
        }
    }
}
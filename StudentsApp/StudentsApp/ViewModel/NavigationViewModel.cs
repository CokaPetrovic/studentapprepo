﻿using StudentsApp.DataService;
using StudentsApp.Event;
using StudentsApp.Events;
using StudentsApp.Model;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Prism.Events;

namespace StudentsApp.ViewModel
{
    public class NavigationViewModel : ViewModelBase, INavigationViewModel
    {
        private StudentDataService studentApi;

        private IEventAggregator _eventAggregator;

        public ObservableCollection<NavigationItemViewModel> Students { get; }

        public NavigationViewModel(IEventAggregator eventAggregator, StudentDataService api)
        {
            studentApi = api;
            Students = new ObservableCollection<NavigationItemViewModel>();
            _eventAggregator = eventAggregator;
            _eventAggregator.GetEvent<AfterDetailSavedEvent>().Subscribe(AfterDetailSaved);
            _eventAggregator.GetEvent<AfterDetailDeletedEvent>().Subscribe(AfterDetailDeleted);

        }

        private void AfterDetailDeleted(AfterDetailDeletedEventArgs obj)
        {
            var item = Students.SingleOrDefault(s => s.Id == obj.Id);
            if (item != null)
            {
                Students.Remove(item);
            }
        }

        private async void AfterDetailSaved(AfterDetailSavedEventArgs obj)
        {
            //var lookUpItem = Students.SingleOrDefault(s => s.Id == obj.Id);
            //if (lookUpItem == null)
            //{
            //    Students.Add(new NavigationItemViewModel(_eventAggregator, obj.Id, obj.Name));
            //}

           await LoadAsync();
        }

        public String DisplayMember { get; set; }
        public async Task LoadAsync()
        {
            Students.Clear();

            var lookup = await studentApi.GetAllAsync();
            
            foreach (var item in lookup)
            {
                Students.Add(new NavigationItemViewModel(_eventAggregator, item.student_id, item.Name));
            }
        }
    }
}

﻿using System.Threading.Tasks;

namespace StudentsApp.ViewModel
{
    public interface IDetailViewModel
    {
        bool HasChanges { get; }
        int Id { get; }

        Task LoadAsync(int id);
    }
}
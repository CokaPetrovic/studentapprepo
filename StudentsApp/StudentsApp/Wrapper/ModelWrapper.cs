﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace StudentsApp.Wrapper
{
    public class ModelWrapper<T> : NotifyDataErrorInfoBase
    {
        public T Model { get; }
        public ModelWrapper(T model)
        {
            Model = model;
        }
        protected virtual TValue GetValue<TValue>([CallerMemberName]string propertyName = null)
        {

            return (TValue)typeof(T).GetProperty(propertyName).GetValue(Model);
        }

        //ZOVE SE KAD JE PROPERTY NASEG STUDENT WRAPPER-A PROMENJEN
        protected virtual void SetValue<TValue>(TValue value, [CallerMemberName]string propertyName = null)
        {
            typeof(T).GetProperty(propertyName).SetValue(Model, value);
            OnPropertyChanged(propertyName);//ne zaboraviiiiii
            ValidatePropertyInternal(propertyName, value);
        }

        private void ValidatePropertyInternal(string propertyName, object value)
        {
            ClearErrors(propertyName);
            ValidateDataAnnotations(propertyName, value);
            ValidateCustomErrors(propertyName);
        }

        private void ValidateDataAnnotations(string propertyName, object value)
        {
            var results = new List<ValidationResult>();
            var contexdt = new ValidationContext(Model) { MemberName = propertyName };
            Validator.TryValidateProperty(value, contexdt, results);

            foreach (var res in results)
            {
                AddError(propertyName, res.ErrorMessage);
            }
        }

        private void ValidateCustomErrors(string propertyName)
        {
            var errors = ValidateProperty(propertyName);
            if (errors != null)
            {
                foreach (var e in errors)
                {
                    AddError(propertyName, e);
                }
            }
        }

        //IEnumerable<string> jer su nam greske u tom obliku..
        protected virtual IEnumerable<string> ValidateProperty(string propertyName)
        {
            return null;
        }
    }
}

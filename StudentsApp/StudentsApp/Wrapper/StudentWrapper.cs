﻿using StudentsApp.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace StudentsApp.Wrapper
{
    public class StudentWrapper : ModelWrapper<Student>
    {
        public StudentWrapper(Student model) : base(model)
        {
        }

        public int Id
        {
            get
            {
                return Model.student_id;
            }
        }
        public string Name
        {
            get
            {
                return GetValue<string>();
            }
            set
            {
                SetValue<string>(value);
            }
        }



        public int Age
        {
            get
            {
                return GetValue<int>();
            }
            set
            {
                SetValue<int>(value);

            }

        }

        public string Image
        {
            get
            {
                return GetValue<string>();
            }
            set
            {
                SetValue<string>(value);

            }
        }

        public string Email
        {
            get
            {
                return GetValue<string>();
            }
            set
            {
                SetValue<string>(value);

            }
        }
        public DateTime DateCreated
        {
            get
            {
                return GetValue<DateTime>();
            }
            set
            {
                SetValue<DateTime>(value);

            }
        }

        protected override IEnumerable<string> ValidateProperty(string propertyName)
        {
            switch (propertyName)
            {
                case nameof(Name):
                    if (string.Equals(Name, "", StringComparison.OrdinalIgnoreCase))
                    {
                        yield return "You have to insert valid name";
                    }
                    break;
            }

        }
    }


}

﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace StudentsApp.DataService
{
    public class BasicResponse<T>
    {

        [JsonProperty(PropertyName = "data")]
        public List<T> data
        { get; set; }
        [JsonProperty(PropertyName = "success")]
        public bool success { get; set; }
        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }
        [JsonProperty(PropertyName = "error_code")]
        public int errorCode { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;

namespace StudentsApp.Model
{
    public class Student
    {

        [JsonProperty(PropertyName = "student_id")]
        public int student_id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public String Name { get; set; }

        [JsonProperty(PropertyName = "age")]
        public int Age { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "image")]
        public string Image { get; set; }

        [JsonProperty(PropertyName = "date_created")]
        public DateTime DateCreated { get; set; }
    }


    public class StudentToSend
    {

        public StudentToSend(Student s)
        {
            Name = s.Name;
            Age = s.Age;
            Email = s.Email;
            Image = s.Image;
        }

        [JsonProperty(PropertyName = "name")]
        public String Name { get; set; }

        [JsonProperty(PropertyName = "age")]
        public int Age { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "image")]
        public string Image { get; set; }

    }
}

﻿using Newtonsoft.Json;
using StudentsApp.Model;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace StudentsApp.DataService
{
    public class StudentDataService
    {
        private static readonly HttpClient client = new HttpClient();
        public async Task<List<Student>> GetAllAsync()
        {
            using (HttpClient client = new HttpClient())
            {
                string uri = "https://apingweb.com/api/rest/students";
                HttpResponseMessage response = await client.GetAsync(uri);

                string content = response.Content.ReadAsStringAsync().Result;

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var toConvert = content.Substring(content.IndexOf('['));
                    toConvert = toConvert.Substring(0, toConvert.Length - 1);
                    return JsonConvert.DeserializeObject<List<Student>>(toConvert);

                }

                else return null;

            }
        }

        public async Task<Student> GetByIdAsync(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                string uri = "https://apingweb.com/api/rest/student/" + id;
                HttpResponseMessage response = await client.GetAsync(uri);

                string content = response.Content.ReadAsStringAsync().Result;

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    BasicResponse<Student> basicResponse = JsonConvert.DeserializeObject<BasicResponse<Student>>(content);
                    //return JsonConvert.DeserializeObject<StudentDTO>(basicResponse.data);
                    return basicResponse.data.FirstOrDefault();
                }

                else return null;

            }
        }

        public async Task CreateOrUpdate(Student s)
        {
            if (s.student_id == 0)
            {
                var toSend = JsonConvert.SerializeObject(s);
                var httpContent = new StringContent(toSend);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = await client.PostAsync("https://apingweb.com/api/rest/student/create", httpContent);
                var responseString = await response.Content.ReadAsStringAsync();
            }
            else
            {
                string id = s.student_id.ToString();
                var toSend = JsonConvert.SerializeObject(s);
                var httpContent = new StringContent(toSend);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = await client.PutAsync("https://apingweb.com/api/rest/student/edit/"+id, httpContent);
                var responseString = await response.Content.ReadAsStringAsync();

            }
        }
        public async Task Remove(int id)
        {
            if (id != 0)
            {
                var response = await client.DeleteAsync("https://apingweb.com/api/rest/student/delete/" + id.ToString());
                var responseString = await response.Content.ReadAsStringAsync();
            }
        }
    }
}
